/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define, brackets, $ */


define(function (require, exports, module) {
	"use strict";

	var CONST = {
		COMMAND_ID:   "netdigo.autoheader",
		COMMAND_NAME: "Insert Header",
		DIALOG_TITLE: "Insert Header Configuration",
		NOT_CFG_MSG:  "You must configure the Insert Header before using it.",
		DATE_VAR:     "${DATE}",
		AUTHOR_VAR:   "${AUTHOR}",
		PROJECT_VAR:  "${PROJECT}",
		HOTKEY:       "Alt-C"
	},
		CommandManager = brackets.getModule("command/CommandManager"),
		EditorManager  = brackets.getModule("editor/EditorManager"),
		Menus          = brackets.getModule("command/Menus"),
		Dialogs        = brackets.getModule("widgets/Dialogs"),
		DefaultDialogs = brackets.getModule("widgets/DefaultDialogs"),
		Config         = require("config/config"),
		config         = new Config();

	function Setheader() {
		var data = config.get();

		switch (data.status) {
			case "not-configured":
				Dialogs.showModalDialog(DefaultDialogs.DIALOG_ID_INFO, CONST.DIALOG_TITLE, CONST.NOT_CFG_MSG);
				config.showPanel();
				return;
		}

		var editor = EditorManager.getFocusedEditor();

		if (editor) {
			if (editor.document.getLine(0) == data.text.split('\n')[0]) {
				editor.document.replaceRange(data.text + "\n", {line: 0, ch: 0},{line: data.length+1, ch: 0});
			} else if (editor.document.getLine(0) == '<?php') {
				if (editor.document.getLine(1) == data.text.split('\n')[0]) {
					editor.document.replaceRange(data.text + "\n", {line: 1, ch: 0},{line: data.length+2, ch: 0});
				} else {
					editor.document.replaceRange(data.text + "\n", {line: 1, ch: 0});
				}
			} else {
				editor.document.replaceRange(data.text + "\n", {line: 0, ch: 0});
			}
		}
	}

	var editMenu = Menus.getMenu(Menus.AppMenuBar.EDIT_MENU);

	CommandManager.register(CONST.COMMAND_NAME, CONST.COMMAND_ID, Setheader);
	editMenu.addMenuItem(CONST.COMMAND_ID, CONST.HOTKEY);
});
