/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define, brackets, $ */
define(function (require, exports, module) {
    "use strict";

    var CONST = {
            PANEL_ID:         "netdigo-autoheader-panel",
		BUTTON_ID:        "netdigo-autoheader-btn",
		CLOSE_ID:         "netdigo-autoheader-close",
		SAVE_ID:          "netdigo-autoheader-save",
		AUTHOR_ID:        "netdigo-autoheader-author",
		COMMENT_CMD_ID:   "netdigo-autoheader-commentCmd",
		TEXT_ID:          "netdigo-autoheader-text",
            DATE_VAR:         "${DATE}",
			AUTHOR_VAR:       "${AUTHOR}",
			PROJECT_VAR:      "${PROJECT}",
			FILE_VAR:         "${FILE}",
            AUTHOR_PREF:      "author",
            COMMENT_CMD_PREF: "commentCmd",
            TEXT_PREF:        "text"
        },
        WorkspaceManager = brackets.getModule("view/WorkspaceManager"),
        ExtensionUtils   = brackets.getModule("utils/ExtensionUtils"),
        Resizer          = brackets.getModule("utils/Resizer"),
        PrefManager      = brackets.getModule("preferences/PreferencesManager"),
		ProjectManager   = brackets.getModule("project/ProjectManager"),
		EditorManager    = brackets.getModule("editor/EditorManager"),
        Preferences      = PrefManager.getExtensionPrefs("joeireland.copyright"),
        DefaultLicense   = require("text!./text/default-license.txt"),
        Panel            = require("text!./html/panel.html"),
        Button           = require("text!./html/button.html"),
        $button,
        $panel;

    /**
    * Constructor to create Copyright Config manager.
    *
    * @constructor
    */
    function Config() {
        ExtensionUtils.loadStyleSheet(module, "css/main.less");

        $("#main-toolbar .buttons").append(Button);
        $button = $("#" + CONST.BUTTON_ID).on("click", this.togglePanel.bind(this));

        WorkspaceManager.createBottomPanel(CONST.PANEL_ID, $(Panel), 600);
        $panel = $("#" + CONST.PANEL_ID);
        $("#" + CONST.CLOSE_ID).on("click", this.togglePanel.bind(this));
        $("#" + CONST.SAVE_ID).on("click", this.save.bind(this));
    }

	function formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear(),
			hours = '' + d.getHours(),
			minute = '' + d.getMinutes();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		if (hours.length < 2) hours = '0' + hours;
		if (minute.length < 2) minute = '0' + minute;

		return year+'-'+month+'-'+day+' '+hours+':'+minute;
	}

    Config.prototype.get = function () {
        var author  = Preferences.get(CONST.AUTHOR_PREF),
            lines   = Preferences.get(CONST.TEXT_PREF);

        if (!author || this.isTextUnset(lines)) {
            return {status: "not-configured"};
        }

		var editor = EditorManager.getFocusedEditor();

		var today  = formatDate(new Date()),
            length = lines.length - 1,
            width  = lines[length].length,
			projectRoot = ProjectManager.getProjectRoot(),
			projectName = projectRoot._name,
			projectPath = projectRoot._path,
			documentPath = editor.document.file.toString(),
            text   = lines.join("\n")
						.replace(CONST.DATE_VAR, today)
						.replace(CONST.AUTHOR_VAR, author)
						.replace(CONST.PROJECT_VAR, projectName)
						.replace(CONST.FILE_VAR, documentPath.replace(projectPath, '').replace('[File ', '').replace(']', ''));

        return { text: text, length: length, width: width, status: "ok" };
    };

    Config.prototype.save = function () {
        Preferences.set(CONST.AUTHOR_PREF, $("#" + CONST.AUTHOR_ID).val());
        Preferences.set(CONST.COMMENT_CMD_PREF, $("#" + CONST.COMMENT_CMD_ID).val());
        Preferences.set(CONST.TEXT_PREF, $("#" + CONST.TEXT_ID).val().split("\n"));
        this.togglePanel();
    };

    Config.prototype.update = function () {
        var author  = Preferences.get(CONST.AUTHOR_PREF);
        var text    = Preferences.get(CONST.TEXT_PREF);

        if (this.isTextUnset(text)) {
            text = DefaultLicense;
        } else {
            text = text.join("\n");
        }

        $("#" + CONST.AUTHOR_ID).val(author);
        $("#" + CONST.TEXT_ID).val(text);
    };

    Config.prototype.showPanel = function () {
        if (!$button.hasClass("active")) {
            this.togglePanel();
        }
    };

    Config.prototype.togglePanel = function () {
        Resizer.toggle($panel);
        $button.toggleClass("active");

        if ($button.hasClass("active")) {
            this.update();
        }
    };

    Config.prototype.isTextUnset = function (text) {
        return !text || text.length === 0 || (text.length === 1 && text[0].length === 0);
    };

    module.exports = Config;
});
